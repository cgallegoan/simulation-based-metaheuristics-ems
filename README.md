# Simulation-based Metaheuristics in EMS

Repositorio de datos, modelos y resultados del Trabajo Fin de Grado de Carlos Gallego Andreu: Diseño y análisis de metaheurísticas basadas en simulación para la optimización de la localización estática de vehículos de emergencias sanitarias.
