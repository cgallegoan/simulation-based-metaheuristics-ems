
module MyFunctions
using FileIO
using DelimitedFiles
using Dates
using Random
using JEMSS

distance_matrix = readdlm("../distance_matrix.csv", ',', Float64);
near_stations = [findall(x -> x < 300, distance_matrix[i, :]) for i in 1:277];


shared_sim = initSim("../valencia_metro/data/base/configs/base.xml");

dates_test = ["5/1", "12/1", "6/1", "2/1", "3/1", "17/1", "23/1", "29/1", "31/1","2/2", "16/2", "3/2", "5/2", "12/2", "19/2", "22/2", "26/2", "27/2","2/3", "16/3", "10/3", "5/3", "12/3", "19/3", "22/3", "26/3", "27/3","6/4", "13/4", "7/4", "2/4", "3/4", "17/4", "23/4", "29/4", "30/4","4/5", "18/5", "5/5", "2/5", "07/5", "15/5", "23/5", "28/5", "30/5","1/6", "15/6", "9/6", "11/6", "18/6", "19/6", "25/6", "26/6", "27/6","6/7", "13/7", "7/7", "2/7", "3/7", "17/7", "23/7", "29/7", "31/7","3/8", "17/8", "4/8", "5/8", "12/8", "19/8", "22/8", "26/8", "27/8","7/9", "14/9", "1/9", "3/9", "10/9", "17/09", "23/9", "26/9", "30/9","5/10", "19/10", "6/10", "1/10", "8/10", "15/10", "22/10", "28/10", "29/10","02/11", "16/11", "3/11", "5/11", "12/11", "19/11", "22/11", "26/11", "27/11","7/12", "14/12", "1/12", "3/12", "10/12", "17/12", "23/12", "26/12", "31/12"];
dates_test = [string(Date(d * "/2019", "d/m/y")) for d in dates_test];
maxAmbulances = 2*ones(Int64, 277);

function countPhenotype(phenotype::Tuple{Vector{Int64}, Vector{Int64}})::Matrix{Int64}
    """
    Counts the number of times each ambulance station appears in the given vector.
    This is what the genetic algorithm needs.
    This is the reverse of countedAmbulancesToStations.

    phenotype: tuple with the ambulance stations.
    """
    count = zeros(Int64, 2, 277)
    @inbounds @simd for i in eachindex(phenotype)
        for j in phenotype[i]; count[i , j] += 1; end
    end
    return count
end;
generatePhenotype() = countPhenotype((rand(1:277, 10), rand(1:277, 19)));  
check_phenotype(phenotype::Matrix{Int64}, maxAmbulances = maxAmbulances) = all(vec(sum(phenotype, dims=1)) .<= maxAmbulances) && sum(phenotype[1,:]) == 10 && sum(phenotype[2,:]) == 19;
function generatePopulation(calls::Vector, n::Int64 = 100, threads::Bool = true)::Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}
    """
    Generates a population of n phenotypes.

    calls: Vector{Call} with the calls to be used to compute the fitness.
    n: Number of phenotypes in the population.
    threads: Whether to use threads or not.
    1st element of the tuple: fitness of the phenotype.
    2nd element of the tuple: phenotype.
    3rd element of the tuple: number of iterations the phenotype survives in the 
    genetic algorithm (for evaluation purposes).
    4th element of the tuple: time found in the the genetic algorithm.
    """
    population = Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}(undef, n)
    if threads
        Threads.@threads for i in 1:n
            ph = generatePhenotype()
            population[i] = (computeFitness(ph, calls), ph, 0, 0.0)
        end
    else
        for _ in 1:n
            ph = generatePhenotype()
            population[i] = (computeFitness(ph, calls), ph, 0, 0.0)
        end
    end
    return population
end;
function countedAmbulancesToStations(phenotype::Matrix{Int64})::Tuple{Vector{Int64}, Vector{Int64}}
    """
    Transforms a vector of counts of ambulances in each station to a vector of ambulance stations.
    This is what the simulation needs.
    This is the reverse of count.

    phenotype: Vector{Int64} with the number of ambulances in each station.
    """
    phenotype2 = (Vector{Int64}(), Vector{Int64}()) 
    for i in Base.OneTo(2)
        for j in Base.OneTo(277)
            for _ in 1:phenotype[i,j]
                push!(phenotype2[i], j)
            end
        end
    end
    return phenotype2
end;
function phenotypeToAmbulances(phenotype::Matrix{Int64})::Vector{Ambulance}
    """
    Creates ambulances for the simulation 
    from the quantities of ambulances in each station.
    
    phenotype: Matrix{Int64} with the number of ambulances in each station.
    """

	n = 29 # total number of ambulances
    ambulances1, ambulances2 = countedAmbulancesToStations(phenotype)
	ambulances = Vector{Ambulance}(undef, n)
	for i = 1:n
        ambulances[i] = Ambulance()
        ambulances[i].index = i
        if i <= 10
            ambulances[i].stationIndex = ambulances1[i]
            ambulances[i].class = AmbClass(1)
        else
            ambulances[i].stationIndex = ambulances2[i-10]
            ambulances[i].class = AmbClass(2)
        end
	end
	
	return ambulances
end
function createCalls(sim = shared_sim, filepath::String = "../valencia_metro/data/calls/2019/2019.csv")::Vector{Call}
    """
    Creates calls for the simulation.

    sim: Simulation of reference.
    filepath: Path to the file with the calls. Must be a csv file.
    """

    allCalls, _ = readCallsFile(filepath) 
    for c in allCalls
        if c.nearestNodeIndex == nullIndex
            (c.nearestNodeIndex, c.nearestNodeDist) = findNearestNode(sim.map, sim.grid, sim.net.fGraph.nodes, c.location)
        end
    end
    return allCalls
end
function computeFitness(phenotype::Matrix{Int64}, calls::Vector, threads::Bool = true, shared_sim = shared_sim)::Float64
    """
    Computes the fitness of a phenotype.

    shared_sim: Simulation to use for the fitness computation.
    calls: Calls to use for the fitness computation.
    phenotype: Phenotype to compute the fitness of.
    threads: Whether to use threads or not.
    """
    s = 0
    if threads == false
        for callsDay in calls
            sim = initSim2(shared_sim, deepcopy(callsDay), phenotypeToAmbulances(phenotype))
            simulate!(sim)
            callstats = getPeriodStatsList(sim)[1].call #Sacado de writeStatsDictFile -> statsDictFromPeriodStatsList (statistics.jl)
            x = getfield(callstats, :totalResponseDuration)
            y = getfield(callstats, :numCalls)
            s += x/y * 24 * 60
        end
    else
        Threads.@threads for i in eachindex(calls)
            sim = initSim2(shared_sim, deepcopy(calls[i]), phenotypeToAmbulances(phenotype))
            simulate!(sim)
            callstats = getPeriodStatsList(sim)[1].call #Sacado de writeStatsDictFile -> statsDictFromPeriodStatsList (statistics.jl)
            x = getfield(callstats, :totalResponseDuration)
            y = getfield(callstats, :numCalls)
            s += x/y * 24 * 60
        end
    end
    return s/length(calls)
end

function getCallsByDay(sim, month::Int64, day = nothing)::Vector
    """
    Gets the calls of a day.

    sim: Simulation of reference.
    month: Month to get the calls of.
    day: Day to get the calls of. If nothing, gets all the calls of the month.
    """
    month = string(month)
    folder_path = "../valencia_metro/data/calls/2019/$month"
    csv_files = walkdir(folder_path) |> collect |> x -> [x[i][3] for i in eachindex(x)] |> x-> reduce(vcat, x) |> x -> x[2:end] |> list -> sort(list, by = x-> Date(x[1:end-4], "y-m-d"))
    if day === nothing
        v = Vector(undef, length(csv_files))
        for i in eachindex(csv_files)
            path = joinpath(folder_path, "$i", csv_files[i])
            v[i] = createCalls(sim, path)
        end
    else
        v = createCalls(sim, joinpath(folder_path, "$day", csv_files[day]))
    end
    return v
end

function fixing(phenotype::Matrix{Int64})::Matrix{Int64}
    """
    Fixes a phenotype. Adds ambulances to configurations that need them 
    and eliminates excess ambulances when there are too many. 
    The process is random.

    phenotype: Phenotype to fix.
    """
    phenotype = deepcopy(phenotype)
    for i in 1:277 #Eliminate impossible configurations
        if phenotype[1,i] > 2; phenotype[1,i] -= 1; end
        if phenotype[1,i] == 2 && phenotype[2,i] > 0; phenotype[1,i] -= 1; end
        if phenotype[2,i] > 2; phenotype[2,i] -= 1; end
        if phenotype[2,i] == 2 && phenotype[1,i] > 0; phenotype[2,i] -= 1; end
    end
    while sum(phenotype[1,:]) > 10 #Eliminate excess configurations
        phenotype[1, findall(phenotype[1,:] .> 0)[rand(1:end)]] -= 1
    end
    while sum(phenotype[2,:]) > 19 #Eliminate excess configurations
        phenotype[2, findall(phenotype[2,:] .> 0)[rand(1:end)]] -= 1
    end
    while sum(phenotype[1,:]) < 10 #Add missing configurations
        r = rand(1:277)
        if phenotype[1,r] + phenotype[2,r] < 2; phenotype[1, r] += 1; end
    end
    while sum(phenotype[2,:]) < 19 #Add missing configurations
        r = rand(1:277)
        if phenotype[1,r] + phenotype[2,r] < 2; phenotype[2, r] += 1; end
    end
    return phenotype
end

function stopOnLimit(t::Float64, max_time::Int64)::Bool
    """
    Stops the algorithm when the time limit is reached.

    t: Time to check.
    max_time: Maximum time to run the algorithm.
    """
    return t > max_time
end

function endGeneticAlgorithm(population, callsFinal::Vector, percent_final::Float64, n::Int64)
    """
    Auxiliar function to end the genetic algorithm. 
    Computes the final fitness of the survivors and returns the best one.

    population: Population to evaluate.
    callsFinal: Calls to use for the final fitness computation.
    percent_next_pop: Percentage of the final population to evaluate.
    n: Number of individuals in the population.
    """
    population = [(computeFitness(x[2], callsFinal), x[2], x[3], x[4]) for x in population[1:Int(percent_final * n)]]
    best = sort(population, by = x -> x[1])[1]
    return best
end

function computeDissimilarity(ind1::Matrix{Int64}, ind2::Matrix{Int64})::Float64
    """
    Computes the dissimilarity between two individuals. 
    The dissimilarity is the sum of the absolute differences between the number of 
    ambulances of each type in each configuration.
    Basically, how many ambulances are in different stations.

    ind1: First individual.
    ind2: Second individual.
    """
    return sum(abs.(ind1 .- ind2))
end

function is_in_population(ind::Matrix{Int64}, population::Vector)::Bool
    """
    Auxiliar function to check whether an individual is in the population.

    ind: Individual to check.
    population: Population to compare with.
    """
    return ind in population
end

function initialise_population(nC1::Int64, nC2::Int64, calls::Vector, total_n::Int64)::Tuple{Vector{Any}, Vector{Any}}
    """
    Initialises the population of the genetic algorithm.

    nC1: Number of individuals of type 1 (high fitness).
    nC2: Number of individuals of type 2 (high dissimilarity).
    calls: Subset of calls to use for the fitness computation.
    """
    population = []
    while length(population) < total_n
        ind = generatePhenotype()
        if !is_in_population(ind, population)
            push!(population, (computeFitness(ind, calls), ind, 1, 0.0))
        end
    end
    highest_fitted = sort(population, by = x -> x[1])
    C1, C2, C2_possible = highest_fitted[1:nC1], [], highest_fitted[nC1+1:end]
    for ind in C2_possible
        dissimilarity = sum([computeDissimilarity(ind[2], c[2]) for c in C1])
        push!(C2, (dissimilarity, ind[2], 1, 0.0))
    end
    C2 = sort(C2, by = x -> x[1], rev = true)[1:nC2]
    return C1, C2
end

# function crossoverCarlos(parents::Vector{Tuple{Float64, Matrix{Int64}, Int64}}, n::Int64)::Vector{Any}
#     """
#     Performs a crossover on a set of parents.

#     parents: Parents to perform the crossover.
#     n: Number of children to generate.
#     """
#     children = Vector{Matrix{Int64}}(undef, n)
#     for i in 1:n; children[i] = zeros(Int64, 2, 277); end

#     for i in 1:2
#         for index in 1:277
#             values = rand(vcat(fill(parents[1][2][i, index], parents[1][3]),fill(parents[2][2][i, index], parents[2][3])), n)
#             for child in 1:n; children[child][i, index] = values[child]; end
#         end
#     end
#     return children
# end

function crossoverBetter(parents::Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}, n_children::Int64, n_groups::Int64, near_stations::Vector = near_stations)::Vector{Any}
    """
    Performs a crossover on a set of parents. 
    It takes advantage of the closeness between stations to interchange groups of stations. 

    parents: Parents to perform the crossover.
    n_children: Number of children to generate.
    n_groups: Number of groups to change.
    near_stations: Vector with the stations close to each station.
    """

    children = []
    for _ in 1:n_children
        groups = rand(1:277, n_groups)
        near_stations_groups = near_stations[groups]
        stations_flipped = unique(vcat(near_stations_groups...))
        child1 = copy(parents[1][2])
        child2 = copy(parents[2][2])
        Threads.@threads for station in stations_flipped
            child1[:, station] = parents[2][2][:, station]
            child2[:, station] = parents[1][2][:, station]
        end
        push!(children, fixing(child1))
        push!(children, fixing(child2))
    end
    return children
end


function mutationSimple(phenotype::Matrix{Int64}, n::Int64 = 1)::Matrix{Int64}
    """
    Performs a mutation on a phenotype by adding 1 ambulance to n random positions.
    The phenotype will need to be fixed after the mutation.

    phenotype: Phenotype to mutate.
    n: Number of mutations to perform.
    """

    pos = rand(1:554, n)
    Threads.@threads for index in pos
        phenotype[index] += 1
    end
    return phenotype
end

function RandomSearch(max_time::Int64, calls::Vector)
    """
    Runs the random search algorithm.

    max_time: Maximum time to run the algorithm.
    calls: Vector with all the calls.
    """
    t1 = time()
    best = Inf
    phenotype = nothing
    list_bests = []
    list_times = []
    while !stopOnLimit(time() - t1, max_time)
        #generate 16 random phenotypes because we have 16 threads
        phenotypes = [generatePhenotype() for _ in 1:16]
        fitnesses = Vector{Float64}(undef, length(phenotypes))
        Threads.@threads for i in eachindex(phenotypes)
            fitnesses[i] = computeFitness(phenotypes[i], calls)
        end
        bestPhenotype = phenotypes[argmin(fitnesses)]
        if min(fitnesses...) < best
            best = min(fitnesses...)
            push!(list_bests, best)
            push!(list_times, time() - t1)
            phenotype = bestPhenotype
        end
    end
    return phenotype, best, list_bests, list_times
end

function getCalls(shared_sim = shared_sim)::Vector
    """
    Gets the calls of each day individually.

    shared_sim: Shared simulation with a loaded road network.
    """
    c = []
    for i in 1:12
        callsTest = getCallsByDay(shared_sim, i);
        append!(c, callsTest);
    end
    return c[filter(i -> isassigned(c, i), 1:length(c))];
end
c = getCalls()
cFinal = createCalls()

function tournamentSelection(population, tournamentSel::Float64, tournamentPart::Int64, bestProb::Float64)::Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}
    """
    Performs a tournament selection on a population.

    population: Population to perform the selection.
    tournSel: Percentage of the population to select.
    tournamentPart: How many individuals to select in each tournament.
    bestProb: Probability of selecting the best individual in the tournament.
    """
    n_selected = Int64(ceil(tournamentSel * length(population)))
    n_selected = n_selected % 2 == 0 ? n_selected : n_selected + 1 #make it even
    selected = Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}(undef, n_selected)
    available_indices = collect(1:length(population))
    Threads.@threads for i in eachindex(selected)
        tournament = rand(available_indices, tournamentPart)
        best = argmin([population[i][1] for i in tournament])
        if rand() < bestProb
            selected[i] = population[tournament[best]]
            #create available indices without the best 
            available_indices = filter(i -> i != tournament[best], available_indices)
        else
            splice!(tournament, best)
            random_index = rand(tournament)
            selected[i] = population[random_index]
            #create available indices without the random index
            available_indices = filter(i -> i != random_index, available_indices)
        end
    end
    return selected
end


function runGABetter(treeParzen, total_n::Int64 = 100, max_iters::Int64 = 9999999, max_repeated_iters::Int64 = 999999, final_percentage::Float64 = 1.0, max_time::Int64 = 600, calls::Vector = c, callsFinal = cFinal)
    """
    Runs the genetic algorithm. To use in combination with the treeParzen library which
    allows for bayesian optimization of the hyperparameters.
        
    treeParzen: Hyperparameters of the algorithm.
        nC1: Number of individuals of type 1.
        nC2: Number of individuals of type 2.
        ncalls: Number of calls to use on each iteration.
        n_groups_cross: Number of groups of close stations to use on the crossover.
        n_children: Number of pairs of children to generate on each crossover.
        mutation_prob: Probability of a mutation happening.
        tournamentSel: Percentage of the population that will survive the tournament.
        tournamentPart: How many individuals to use in each tournament.
        bestProb: Probability of selecting the best individual in the tournament.
        
    total_n: Number of individuals to generate at the start.
    max_iters: Maximum number of iterations to run the algorithm for.
    max_repeated_iters: Maximum number of iterations without improvement to run the algorithm for.
    final_percentage: Percentage of the calls to use on the final evaluation.
    max_time: Maximum time to run the algorithm for.
    calls: Each element contains the calls of a day.
    callsFinal: Calls to use on the final evaluation.
    """

    nC1 = Int(treeParzen[:nC1]); nC2 = Int(treeParzen[:nC2]); ncalls = Int(treeParzen[:ncalls]); n_groups_cross = Int(treeParzen[:n_groups_cross]); n_children = Int(treeParzen[:n_children]); mutation_prob = treeParzen[:mutation_prob]; tournamentSel = treeParzen[:tournamentSel]; tournamentPart = Int(treeParzen[:tournamentPart]); bestProb = treeParzen[:bestProb]

    t1 = time()    
    callsIter = calls[rand(eachindex(calls), ncalls)]
    C1, C2 = initialise_population(nC1, nC2, callsIter, total_n)
    population = vcat(C1, C2)

    list_bests = [C1[1][1]]
    list_times = [0.0]
    iters = 0

    while (max_iters > 0) & (maximum(map(x -> x[3], population)) < max_repeated_iters) & !stopOnLimit(time() - t1, max_time)

        lengthNextPop = Int(round(length(population)))
        next_pop = Vector{Tuple{Matrix{Int64}, Int64, Float64}}(undef, lengthNextPop)
        Threads.@threads for i in eachindex(next_pop); next_pop[i] = (population[i][2], population[i][3], population[i][4]); end

        possible_parents = tournamentSelection(population, tournamentSel, tournamentPart, bestProb)
        parents = [(possible_parents[i], possible_parents[i+1]) for i in 1:2:length(possible_parents)]

        lenghtNextPopSons = Int(round(length(parents) * (n_children*2)))
        next_pop_sons = Vector{Tuple{Matrix{Int64}, Int64, Float64}}(undef, lenghtNextPopSons)

        Threads.@threads for i in eachindex(parents)
            children = crossoverBetter([parents[i][1], parents[i][2]], n_children, n_groups_cross)
            if rand() < mutation_prob
                childrenV = Vector{Matrix{Int64}}(undef, length(children))
                Threads.@threads for j in eachindex(children); childrenV[j] = mutationSimple(children[j]); end
                children = childrenV
            end
            Threads.@threads for k in eachindex(children); next_pop_sons[(i-1)*(n_children*2)+k] = (fixing(children[k]), 0, time()-t1); end
        end
        next_pop = vcat(next_pop, next_pop_sons)

        callsIter = calls[rand(eachindex(calls), ncalls)]
        next_popfit = Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}(undef, length(next_pop))
        Threads.@threads for i in eachindex(next_pop)
            phenotype = next_pop[i][1]
            next_popfit[i] = (computeFitness(phenotype, callsIter), phenotype, next_pop[i][2] + 1, next_pop[i][3])
        end

        sort!(next_popfit, by = x -> x[1])
        C1 = next_popfit[1:nC1]
        push!(list_bests, C1[1][1])
        push!(list_times, time() - t1)
        iters += 1

        C2_possible = next_popfit[nC1+1:end]
        C2 = Vector{Tuple{Float64, Float64, Matrix{Int64}, Int64, Float64}}(undef, length(C2_possible))

        Threads.@threads for i in eachindex(C2_possible)
            dissimilarity = sum([computeDissimilarity(C2_possible[i][2], c[2]) for c in C1])
            C2[i] = (dissimilarity, C2_possible[i][1], C2_possible[i][2], 1, 0.0)
        end

        C2 = sort(C2, by = x -> x[1], rev = true)[1:nC2]
        #remove C2[i][1] for i in eachindex(C2)
        C2 = [(C2[i][2], C2[i][3], C2[i][4], C2[i][5]) for i in eachindex(C2)]
        population = vcat(C1, C2)
        max_iters -= 1

    end
    best = endGeneticAlgorithm(C1, [callsFinal], final_percentage, nC1)
    best_value = best[1]; best_phenotype = best[2]; best_iter = best[3]; best_timefound = best[4]
    best_phenotype = string(best_phenotype)
    list_bests = string(list_bests)
    list_times = string(list_times)
    #open("resultsGenetic.txt", "a") do f
    #    write(f, "$nC1;$nC2;$ncalls;$n_groups_cross;$n_children;$mutation_prob;$best_value;$best_iters;$best_timefound;$best_phenotype;$list_bests;$list_times;$iters\n")
    #end
    return (best_value, best_phenotype, best_iter, best_timefound, iters, list_bests, list_times)
end

function neighbourhood(ind::Matrix{Int64})::Vector{Tuple{Matrix{Int64}, Matrix{Int64}}}
    """
    Generates the full neighbourhood of an individual.
    The neighbourhood is considered to be the set of individuals that can be generated 
    by changing one ambulance from one station to another.

    ind: Individual to generate the neighbourhood from.
    """
    neighbours = Vector{Tuple{Matrix{Int64}, Matrix{Int64}}}(undef, 0)
    for i in 1:size(ind, 1)
        for j in 1:size(ind, 2)
            if ind[i, j] > 0  #That index has to be changed
                for ii in 1:size(ind, 1)
                    for jj in 1:size(ind, 2)
                        newInd = copy(ind)
                        newInd[i, j] -= 1
                        newInd[ii, jj] += 1
                        if check_phenotype(newInd)
                            push!(neighbours, (newInd, ind - newInd))
                        end
                    end
                end
            end
        end
    end                
    return neighbours[filter(i -> isassigned(neighbours, i), 1:length(neighbours))]
end

function neighboursFitness(neighbours::Vector{Tuple{Matrix{Int64}, Matrix{Int64}}}, calls::Vector, percent_neighbours::Float64)::Vector{Tuple{Matrix{Int64}, Float64, Vector{Int64}}}
    """
    Computes the fitness of a certain % of the neighbours.

    neighbours: Neighbours to compute the fitness of.
    calls: Calls to compute the fitness with.
    percent_neighbours: Percentage of neighbours to compute the fitness of.
    """ 

    neighbours = neighbours[shuffle(Random.default_rng(), eachindex(neighbours))][1:trunc(Int, percent_neighbours * length(neighbours))]
    # println("Number of neighbours to compute: $(length(neighbours))")
    neighboursFitted = Vector{Tuple{Matrix{Int64}, Float64, Vector{Int64}}}(undef, length(neighbours))
    Threads.@threads for neighbour in neighbours
        fitness = computeFitness(neighbour[1], calls)
        positions = [i for i in 1:length(neighbour[2]) if neighbour[2][i] != 0]
        push!(neighboursFitted, (neighbour[1], fitness, positions))
    end
    return neighboursFitted[filter(i -> isassigned(neighboursFitted, i), 1:length(neighboursFitted))]
end

function tabuSearchAlgorithm(n::Int64, neighb_p::Float64, ncalls::Int64, max_tabu_size::Int64, cFinal = cFinal, calls::Vector = c, max_time::Int64 = 2000)
    """
    Tabu search algorithm.

    n: Number of individuals to generate in the initial population.
    neighb_p: Percentage of neighbours to compute the fitness of.
    ncalls: Number of calls to compute the fitness with.
    max_tabu_size: Maximum size of the tabu list.
    cFinal: Vector of calls to compute the fitness with at the end of the algorithm.
    calls: Each element contains the calls of a day.
    max_time: Maximum time to run the algorithm for.
    """
    t1 = time()
    initial_individuals = [generatePhenotype() for _ in 1:n]
    callsIter = calls[rand(eachindex(calls), ncalls)]

    initial_individualsFitted = Vector{Tuple{Matrix{Int64}, Float64, Float64, Int64}}()
    Threads.@threads for individual in initial_individuals
        fitness = computeFitness(individual, callsIter)
        push!(initial_individualsFitted, (individual, fitness, 0.0, 0))
    end

    best_individual = sort(initial_individualsFitted, by = x -> x[2])[1]
    individual = best_individual[1]
    tabu_list = []
    list_bests = [best_individual[2]]
    list_times = [0.0]
    iter = 0

    while !stopOnLimit(time() - t1, max_time)
        iter += 1
        callsIter = calls[rand(eachindex(calls), ncalls)]
        neighbours = neighbourhood(individual)
        neighboursFitted = neighboursFitness(neighbours, callsIter, neighb_p)
        neighboursFitted = sort(neighboursFitted, by = x -> x[2])

        while neighboursFitted[1][3] in tabu_list
            popfirst!(neighboursFitted) #delete the move that is in the tabu list
        end
        best_neighbour = neighboursFitted[1]
        push!(tabu_list, best_neighbour[3]); push!(tabu_list, reverse(best_neighbour[3]))

        if best_neighbour[2] < best_individual[2]
            best_individual = (best_neighbour[1], best_neighbour[2], time() - t1, iter)
            push!(list_bests, best_individual[2])
            push!(list_times, best_individual[3])
        end
        if length(tabu_list) > max_tabu_size
            popfirst!(tabu_list)
        end
        individual = best_neighbour[1]
    end
    return (best_individual[1], computeFitness(best_individual[1], [cFinal]), best_individual[3], best_individual[4], iter, list_bests, list_times)
end

function onePointCrossover(parent1::Matrix{Int64}, parent2::Matrix{Int64})::Tuple{Matrix{Int64}, Matrix{Int64}}
    """
    Performs one point crossover between two parents.

    parent1: First parent.
    parent2: Second parent.
    """
    j = rand(1:277)
    child1 = hcat(parent1[:,1:j], parent2[:,j+1:end])
    child2 = hcat(parent2[:,1:j], parent1[:,j+1:end])
    return child1, child2
end

#Algoritmo paper
function runGA(ncalls::Int64, n::Int64 = 20, percent_next_pop::Float64 = 3/20, percent_crossover::Float64 = 14/20, max_iters::Int64 = 99999999, max_repeated_iters::Int64 = 999999999, callsFinal::Vector = cFinal, calls::Vector = c, max_time::Int64 = 600)
    """
    Runs the genetic algorithm.

    n: Number of individuals in the population.
    percent_next_pop: Percentage of the population that will be in the next generation.
    percent_crossover: Percentage of the population that will be the result of crossover.
    percent_mutate: Percentage of the population that will be the result of mutation.
    max_generations: Maximum number of generations to run the algorithm.
    max_counter: Maximum number of generations without improvement to run the algorithm.
    callsFinal: Calls to test the algorithm.
    max_time: Maximum time to run the algorithm.
    """
    t1 = time()
    population = generatePopulation([callsFinal], n)
    sort!(population, by = x -> x[1])
    best_inds = Int(percent_next_pop * n)
    cross_inds = Int(percent_crossover * n)
    list_bests = [population[1][1]]
    list_times = [0.0]
    total_iters = 0
    while (max_iters > 0) & (maximum(map(x -> x[3], population)) < max_repeated_iters) & !stopOnLimit(time() - t1, max_time)
        this_iter_time = time() - t1
        next_pop = Vector{Tuple{Matrix{Int64}, Int64, Float64}}(undef, n)
        next_pop[1:best_inds] = [(x[2], x[3]+1, x[4]) for x in population[1:best_inds]]
        crossoverInds = [x[2] for x in population[(best_inds+1):(cross_inds + best_inds)]]
        mutateInds = [x[2] for x in population[(best_inds+cross_inds+1):end]]
        crossoverParents = shuffle(crossoverInds) 
        crossoverParents = [(crossoverParents[i], crossoverParents[i+1]) for i in 1:2:length(crossoverParents)]

        Threads.@threads for i in eachindex(crossoverParents)
            c1, c2 = onePointCrossover(crossoverParents[i][1], crossoverParents[i][2])
            next_pop[best_inds + (i*2)-1] = (c1, 0, this_iter_time)
            next_pop[best_inds + (i*2)] = (c2, 0, this_iter_time)
        end
        Threads.@threads for i in eachindex(mutateInds)
            mutatedInd = mutationSimple(mutateInds[i])
            next_pop[(best_inds+cross_inds+i)] = (mutatedInd, 0, this_iter_time)
        end

        callsIter = calls[rand(eachindex(calls), ncalls)]
        next_popfit = Vector{Tuple{Float64, Matrix{Int64}, Int64, Float64}}(undef, n)
        Threads.@threads for i in eachindex(next_popfit)
            phenotype = next_pop[i][1]
            check_phenotype(phenotype) ? nothing : phenotype = fixing(phenotype)
            next_popfit[i] = (computeFitness(phenotype, callsIter), phenotype, next_pop[i][2], next_pop[i][3])
        end
        population = sort(collect(Set(next_popfit)), by = x -> x[1])[1:n]
        max_iters -= 1
        total_iters += 1
        push!(list_bests, population[1][1])
        push!(list_times, population[1][4])
    end
    best = population[1]
    best_value = best[1]; best_individual = string(best[2]); best_iter = best[3]; best_time = best[4]
    list_bests = string(list_bests)
    list_times = string(list_times)
    return (best_value, best_individual, best_iter, best_time, total_iters, list_bests, list_times)
end

function evaluateRandomIndividuals(calls::Vector = cFinal)
    """
    Evaluates random individuals using 1-100% of the calls.

    n: Number of individuals to evaluate.
    percent_calls: Percentage of calls to use.
    calls: Calls to train the algorithm.
    callsFinal: Calls to test the algorithm.
    """
    for i in 1:100
        quantity = Int(ceil(365 * i / 100))
        time_avg = 0.0
        total_calls = Int(ceil(length(calls) * i / 100))
        callsIter = calls[1:total_calls]
        individual = generatePhenotype()
        for _ in 1:30
            timetook = computeFitness(individual, [callsIter])
            time_avg += timetook
        end
        time_avg = time_avg / 30
        open("computingTime.txt", "a") do f
            write(f, "$quantity;$total_calls;$time_avg\n")
        end
    end
end

end # module

