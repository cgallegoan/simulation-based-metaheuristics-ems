include("../MyFunctions.jl")
cAll = MyFunctions.createCalls(); 

for i in 1:30
    phenotype, best, list_bests, list_times = MyFunctions.RandomSearch(600, [cAll])

    open("../results/resultsRandom.txt", "a") do f
        write(f, "$best;$phenotype\n")
    end

    open("../results/random_experiments/Random$i.txt", "w") do f
        for j in eachindex(list_bests)
            write(f, "$(list_bests[j]);$(list_times[j])\n")
        end
    end
end