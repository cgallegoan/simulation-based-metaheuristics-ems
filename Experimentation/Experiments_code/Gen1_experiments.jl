include("../MyFunctions.jl")

n_llamadasL = [5, 10, 15]

for ncalls in n_llamadasL
    for i in 1:30
        fitness, best_individual, iters_survived, best_timefound, total_iters, list_bests, list_times = MyFunctions.runGA(ncalls)
        open("../results/resultsGen.txt", "a") do f
            write(f, "$ncalls;$fitness;$best_individual;$iters_survived;$best_timefound;$total_iters;$list_bests;$list_times\n")
        end
    end
end