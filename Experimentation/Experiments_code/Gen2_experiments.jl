include("../MyFunctions.jl")

using TreeParzen

space = Dict(
           :nC1 => HP.QuantUniform(:nC1, 10.0, 100.0, 10.0),
           :nC2 => HP.QuantUniform(:nC2, 0.0, 20.0, 2.0),
           :ncalls => HP.QuantUniform(:ncalls, 5.0, 15.0, 5.0),
           :n_children => HP.QuantUniform(:n_children, 1.0, 10.0, 1.0),
           :n_groups_cross => HP.QuantUniform(:n_groups_cross, 1.0, 5.0, 1.0),
           :mutation_prob => HP.Uniform(:mutation_prob, 0.0, 1.0),
           :tournamentSel => HP.Uniform(:tournamentSel, 0.1, 1.0),
           :tournamentPart => HP.QuantUniform(:tournamentPart, 2.0, 10.0, 1.0),
           :bestProb => HP.Uniform(:bestProb, 0.7, 1.0),
)

function test(space)
    values, best_iters, best_timefounds, itersl = [], [], [], []
    nC1 = Int(space[:nC1]); nC2 = Int(space[:nC2]); ncalls = Int(space[:ncalls]); n_groups_cross = Int(space[:n_groups_cross]); n_children = Int(space[:n_children]); mutation_prob = space[:mutation_prob]; tournamentSel = space[:tournamentSel]; tournamentPart = Int(space[:tournamentPart]); bestProb = space[:bestProb]
    for _ in 1:5 
        try
            best_value, best_iter, best_timefound, iters = MyFunctions.runGABetter(space)
        catch e 
            println(e)
            println(nC1, " ", nC2, " ", ncalls, " ", n_groups_cross, " ", n_children, " ", mutation_prob, " ", tournamentSel, " ", tournamentPart, " ", bestProb)
            best_value, best_iter, best_timefound, iters = 99, 99, 999, 99
        end
        #push values to list
        push!(values, best_value)
        push!(best_iters, best_iter)
        push!(best_timefounds, best_timefound)
        push!(itersl, iters)
    end
    best_value = sum(values)/length(values)
    best_iter = sum(best_iters)/length(best_iters)
    best_timefound = sum(best_timefounds)/length(best_timefounds)
    iters = sum(itersl)/length(itersl)

    open("../results/resultsGen2.txt", "a") do f
        write(f, "$nC1;$nC2;$ncalls;$n_groups_cross;$n_children;$mutation_prob;$tournamentSel;$tournamentPart;$bestProb;$best_value;$best_iter;$best_timefound;$iters\n")
    end
    return best_value
end


best_combinations = Dict(
    :d1 => Dict([(:nC1, 10), (:nC2, 0), (:ncalls, 5), (:n_children, 8), (:n_groups_cross, 5), (:mutation_prob, 0.868741), (:tournamentSel, 0.725080), (:tournamentPart, 10), (:bestProb, 0.850768)]),
    :d2 => Dict([(:nC1, 10), (:nC2, 0), (:ncalls, 5), (:n_children, 9), (:n_groups_cross, 5), (:mutation_prob, 0.900681), (:tournamentSel, 0.417582), (:tournamentPart, 9), (:bestProb, 0.979611)]),
    :d3 => Dict([(:nC1, 10), (:nC2, 0), (:ncalls, 5), (:n_children, 10), (:n_groups_cross, 4), (:mutation_prob, 0.715194), (:tournamentSel, 0.509438), (:tournamentPart, 10), (:bestProb, 0.835597)]),
    :d4 => Dict([(:nC1, 10), (:nC2, 0), (:ncalls, 5), (:n_children, 7), (:n_groups_cross, 4), (:mutation_prob, 0.779131), (:tournamentSel, 0.171332), (:tournamentPart, 10), (:bestProb, 0.970528)]),
    :d5 => Dict([(:nC1, 10), (:nC2, 0), (:ncalls, 5), (:n_children, 9), (:n_groups_cross, 5), (:mutation_prob, 0.845135), (:tournamentSel, 0.119180), (:tournamentPart, 10), (:bestProb, 0.993048)]),
)


for (key, value) in best_combinations
    for _ in 1:30
        best_value, best_individual, best_timefound, best_iter, total_iters, list_bests, list_times = MyFunctions.runGABetter(value)
        open("../results/resultsGen2_2.txt", "a") do f
            write(f, "$key;$best_value;$best_individual;$best_timefound;$best_iter;$total_iters;$list_bests;$list_times\n")
        end
    end
end
