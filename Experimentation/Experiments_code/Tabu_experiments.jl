include("../MyFunctions.jl")
cAll = MyFunctions.createCalls(); #All calls at once
c = MyFunctions.getCalls(); #Individual calls

neighb_pL = [0.01, 0.05, 0.1]
n_llamadasL = [5, 10, 15, 20]
tabuL = [5, 10]

for neighb_p in neighb_pL
    for n_llamadas in n_llamadasL
        for tabu in tabuL
            for i in 1:30
                phenotype, best, timet, iter, total_iters, list_bests, list_times = MyFunctions.tabuSearchAlgorithm(100, neighb_p, tabu, n_llamadas, [cAll], c, 600)
                phenotype = string(phenotype)
                list_bests = string(list_bests)
                list_times = string(list_times)
                open("../results/resultsTabu.txt", "a") do f
                    write(f, "$neighb_p;$n_llamadas;$tabu;$best;$phenotype;$timet;$iter;$total_iters;$list_bests;$list_times\n")
                end
            end
        end
    end
end